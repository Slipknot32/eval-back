const bodyParser = require('body-parser');
const express = require('express');
const mysql = require('mysql')
const app = express();

//////////////////////////////////// body parser
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

require(`dotenv`).config();

////////////////////////////////connectionBDD:
const connection = mysql.createConnection({
	host: process.env.DB_HOST,
	user: process.env.DB_USER,
	password: process.env.DB_PASSWORD,
	database: process.env.DB_DATABASE
});

connection.connect((err)=>{
	if (err){
		console.log(err);
		return;
	}
	console.log('ça marche !')
})

////////////////////////////////////// moteur de template
app.set('view engine','pug');

app.use(express.static(`${__dirname}/public`))

///// accueil -> connexion

app.get('/',(req, res) =>{res.render('connexion/login');
});
// redirect button inscription sur tp inscription 

app.get("/connect/inscription",(req,res) =>{res.render("connexion/inscription")});

/////////////////////////////////post inscription base de donnée selon Admin ou user 

app.post('/connect/inscription', (req, res) => {
	let admin = req.body.admin
	let user = req.body.user
	let login = req.body.login

	if (user = user){

		connection.query(`SELECT * FROM user WHERE login ="${login}";`, (err, result, fields) => {
			if (result.length < 1) {
				connection.query(`INSERT INTO user (login, email, password) 
					VALUES 
					('${req.body.login}','${req.body.mail}','${req.body.password}');`);
				res.redirect('/');
			}
			else {
				res.send("login existant")
			}
		})}
		else {
			connection.query(`SELECT * FROM admin WHERE login ="${login}";`, (err, result, fields) => {
				if (result.length < 1) {
					connection.query(`INSERT INTO admin (login, email, password) 
						VALUES 
						('${req.body.login}','${req.body.mail}','${req.body.password}')`);
					res.redirect('/');
				}
				else{
					res.send("login existant")
				};
			})
		}
	});

//////////////////////////////// verification login user et Admin 
app.get("/accueil_user",(req,res) =>{res.render("user/accueil_user")});
app.get("/accueil_admin",(req,res) =>{res.render("admin/accueil")});
app.get("/password_lost",(req,res) =>{res.render("connexion/password_lost")});

app.post('/connect/login', (req, res) => {
	let admin = req.body.admin
	let user = req.body.user
	let loginuser = req.body.login
	let mdpuser = req.body.password

	if (user = user){

		connection.query(` SELECT * FROM user WHERE login ='${loginuser}';`, (err, result, fields) => {
			if (result.length < 1) {
				res.send("login non exitent");
			} 
			else if (loginuser === result[0].login) {

				if (mdpuser === result[0].password){
					res.redirect('/accueil_user');
				}   
				else {
					res.redirect('/password_lost')
				}
			}
			else {
				res.send("page introuvable !!")
			}
		})
	}
	else {
		connection.query(` SELECT * FROM admin WHERE login ='${loginuser}';`, (err, result, fields) => {
			if (result.length < 1) {
				res.send("login non exitent");
			} 
			else if (loginuser === result[0].login) {

				if (mdpuser === result[0].password){
					res.redirect('/accueil_admin');
				}   
				else {
					res.redirect('/password_lost')
				}
			}
			else {
				res.send("page introuvable !!")
			}
		})
	}
});
 /////////////////////////////////////// affichage template admin

 app.get('/connect/admin', (req, res) => {
 	connection.query(`SELECT * FROM posts;`, (err, result, fields) => {
 		if(err){
 			console.log(err)
 			return
 		}
 		console.log(result);
 		res.render('/accueil_admin', {results: result})
 	})
 })

/////////////////////////////////////////////creation defi par l'admin
app.get('/admin/create_defi', (req, res) => res.render('admin/create'))

app.post('/defi', (req, res) => {
	let titre = req.body.title
	let content = req.body.content
	let image = req.body.image

	connection.query(`INSERT INTO posts (title, text, image) 
		VALUES 
		("${req.body.title}", "${req.body.content}", "${req.body.image}");`, (err, result, fields) => {
			if(err){
				console.log(err)
			}
		});
	res.redirect('/accueil_admin');
})

/////////////////////deconnection admin et user

app.get('/admin/sign_up', (req, res) => res.redirect('/'))

app.get('/user/sign_up', (req, res) => res.redirect('/'))

/////////////////////////////////// recupération BDD
// app.get('/post/:id', (req, res) => {
// 	connection.query(`SELECT * FROM posts where id = '${req.params.id}';`, (err, result, fields) => {
// 		if(err){
// 			console.log(err);
// 			return
// 		}

// 		res.render('post/show',{result: result[0]})
// 	})
// })
	app.get('/accueil_user', (req, res) => {
		connection.query(`SELECT * FROM posts;`, (err, result, fields) => {
			if(err){
				console.log(err)
				return
			}
		// console.log(result);
		res.render('/accueil_user', {results: result})
		// res.send("bonjour petit etre !")
	})
	})

	app.get('/accueil_admin', (req, res) => {
		connection.query(`SELECT * FROM posts;`, (err, result, fields) => {
			if(err){
				console.log(err)
				return
			}
		// console.log(result);
		res.render('/accueil_admin', {results: result})
		// res.send("bonjour petit etre !")
	})
	})











// listen on port :

app.listen(process.env.PORT, () => console.log(`Je suis lancé on ${process.env.PORT} !`))